# World Anvil Table Parser

Here is a simple html site that will convert pasted table
data into a format that is consumable by the World Anvil
table creator. 

It expects as an input a table that's first line is the
header for the output. It also expects that the input table
is space seperated. This is usually the case if you copy from
something like a PDF and input it here.

The HTML rendered table is for referance (or embedding).

The table title input (at the top) is for the future, when
we will ba able to upload these tables directly through the
API (when the POST endpoints are live).