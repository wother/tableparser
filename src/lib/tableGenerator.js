import { elt } from "./DOM.js";

export function tableFromArray (inputArray) {
    let table = elt('table');
    
    let keysArray = Object.keys(inputArray[0]);
    let tableHead = elt('thead');
    let tableHeadRow = elt('tr');
    keysArray.forEach((key) =>{
        let tableHeadData = elt('th');
        tableHeadData.innerHTML = key;
        tableHeadRow.appendChild(tableHeadData);
    });
    table.appendChild(tableHead.appendChild(tableHeadRow));

    inputArray.forEach((rowObj, index) => {
        let newRow = elt('tr');
        Object.keys(rowObj).forEach(key =>{
            let newTableDataCell = elt('td');
            newTableDataCell.innerHTML = rowObj[key];
            newRow.appendChild(newTableDataCell);
        });
        table.appendChild(newRow);
    });
    return table;
};

export function arrayToCSV (inputArray) {
    let output = "";
    
    let keysArray = Object.keys(inputArray[0]);
    let headerString = "";
    keysArray.forEach((key) =>{
        headerString += key + "|";
    });
    headerString = headerString.slice(0, headerString.length - 1) + "\n";
    output += headerString;

    inputArray.forEach(tableObj =>{
        let rowString = "";
        Object.keys(tableObj).forEach((key, index) => {
            rowString += `${tableObj[key]}|`;
        });
        output += rowString.slice(0, rowString.length - 1) + "\n";
    });
    return output;
};