export function parseTableData(inputTextString) {
  let output = [];
  let fullArray = inputTextString.split("\n");
  let tableKeysString = fullArray.shift();
  let tableKeysArray = tableKeysString.split(" ");

  fullArray.forEach((row) => {
    let rowArray = row.split(" ");
    let rowObj = {};
    rowArray.forEach((dataCell, index) => {
      rowObj[tableKeysArray[index]] = dataCell;
    });
    output.push(rowObj);
  });
//   console.log('HERE YOU GO!');
//   console.log('-------------');
//   console.log(output);
  return output;
};