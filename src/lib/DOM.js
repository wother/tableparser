export function elt(tag, inputOptions) {
  let output = document.createElement(tag);
  if (inputOptions) {
    let keysArray = Object.keys(inputOptions);
    keysArray.forEach((key) => {
      output[key] = inputOptions[key];
    });
  }
  return output;
}
