import { $$ } from "./lib/selectors.js";
import { parseTableData } from "./lib/parser.js";
import { tableFromArray, arrayToCSV } from "./lib/tableGenerator.js";

$$('#goBtn').addEventListener('click', function(evt){
    let tableData = $$("#pastedInputTextArea").value;
    // debugger;
    let parsedData = parseTableData(tableData);
    $$("#outputDiv").innerHTML = tableFromArray(parsedData).outerHTML;
    $$('#csvOutput').value = arrayToCSV(parsedData);
});
